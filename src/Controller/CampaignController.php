<?php

namespace App\Controller;

use App\Entity\Campaign;
use App\Form\CampaignType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Participant;

/**
 * @Route("/campaign")
 */
class CampaignController extends AbstractController
{

    /**
     * @Route("/", name="campaign_index", methods="GET")
     */
    public function index(): Response
    {
        $campaigns = $this->getDoctrine()
            ->getRepository(Campaign::class)
            ->findAll();

        return $this->render('campaign/index.html.twig', ['campaigns' => $campaigns]);
    }

    /**
     * @Route("/new", name="campaign_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $campaign = new Campaign();
        $form = $this->createForm(CampaignType::class, $campaign);
        $form->handleRequest($request);
        $campaign->setName($request->request->get('name'));

        if (isset($_GET['campaign_title'])){
        $campaign_title = $_GET['campaign_title'];
        }

        // dd($campaign);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $campaign->setId();
            $em->persist($campaign);
            $em->flush();

            
            return $this->redirectToRoute('campaign_show', [
                'id' => $campaign->getId()
            ]);
            }
            
            if (isset($_GET['campaign_title'])){
            return $this->render('campaign/new.html.twig', [
            'campaign' => $campaign,
            'form' => $form->createView(),
            'campaign_title' => $campaign_title,
        ]);
            }else{
                return $this->render('campaign/new.html.twig', [
                    'campaign' => $campaign,
                    'form' => $form->createView(),
                ]);
            }
        }

    /**
     * @Route("/{id}", name="campaign_show", methods="GET")
     */
    public function show(Campaign $campaign): Response
    {
        $participant = new Participant;
        $participants = $this->getDoctrine()->getRepository(Participant::class)->findBy([
            'campaign_id' => $campaign->getId()
        ]);

        $query = 'SELECT participant.*, payment.amount as payment_amount, spending.amount as spending_amount, spending.label as spending_label
        FROM participant
        LEFT JOIN payment ON payment.participant_id = participant.id 
        LEFT JOIN spending ON spending.participant_id = participant.id 
        WHERE campaign_id = "'.$campaign->getId().'"';

        $statement = $this->getDoctrine()->getManager()->getConnection()->prepare($query);
        $statement->execute();

        $participantWithParticipations = $statement->fetchAll();

        //dd($participantWithParticipations);

        $total_amount = 0;
        $total_participant = count($participantWithParticipations);

        for($i = 0 ; $i <= ($total_participant - 1); $i++)
        {
            $total_amount += $participantWithParticipations[$i]["amount"];
        }
        $total_amount2 = $total_amount / 100;
        $objectif = round($total_amount2 / $campaign->getGoal() * 100);

        return $this->render('campaign/show.html.twig', compact('campaign', 'participantWithParticipations', 'total_participant', 'total_amount2', 'objectif'));
    }

    /**
   * @Route("/{id}/pay", name="campaign_pay", methods="GET|POST")
   */
  public function pay(Campaign $pay): Response
  {
        $amountParticipant = $_GET['amountParticipant'] ?? "";
      return $this->render('campaign/pay.html.twig', ['campaign' => $pay]);
  }

    /**
     * @Route("/{id}/spend", name="campaign_spend", methods="GET|POST")
     */
    public function spend(Campaign $campaign): Response
    {
        //$amountParticipant = $_GET['amountParticipant'] ?? "";
        return $this->render('campaign/spend.html.twig', ['campaign' => $campaign]);
    }

    /**
     * @Route("/{id}/edit", name="campaign_edit", methods="GET|POST")
     */
    public function edit(Request $request, Campaign $campaign): Response
    {
        $form = $this->createForm(CampaignType::class, $campaign);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('campaign_show', ['id' => $campaign->getId()]);
        }

        return $this->render('campaign/edit.html.twig', [
            'campaign' => $campaign,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="campaign_delete", methods="DELETE")
     */
    public function delete(Request $request, Campaign $campaign): Response
    {
        if ($this->isCsrfTokenValid('delete'.$campaign->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($campaign);
            $em->flush();
        }

        return $this->redirectToRoute('campaign_index');
    }
}
